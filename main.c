#include <stdio.h>
#include "libs/data_struct/vector/int_vector.h"
#include <assert.h>

void test_pushBack_emptyVector() {
    printf("test_pushBack_emptyVector\n");

    int_vector vec = createIntVector(4);
    printf("is vec empty: %d\n", isIntVectorEmpty(&vec));

    int x = 10;
    pushToIntVector(&vec, x);
    printf("insert %d\n", x);

    printf("is vec empty: %d\n\n", isIntVectorEmpty(&vec));
}


void test_pushBack_fullVector() {
    printf("test_pushBack_fullVector\n");

    int_vector vec = createIntVector(4);
    int input_data[] = {1, 2, 3, 4};
    for (int i = 0; i < 4; i++) {
        pushToIntVector(&vec, input_data[i]);
    }
    printf("is vec full: %d\n", isIntVectorFull(&vec));

    int x = 10;
    pushToIntVector(&vec, x);
    printf("insert %d\n", x);

    printf("is vec full: %d\n\n", isIntVectorFull(&vec));
}


void test_popBack_notEmptyVector() {
    printf("test_popBack_notEmptyVector\n");

    int_vector vec = createIntVector(1);
    pushToIntVector(&vec, 10);

    assert(vec.size == 1);
    popFromIntVector(&vec);
    assert(vec.size == 0);

    assert(vec.capacity == 1);
    printf("OK\n\n");
}


void test_atVector_notEmptyVector() {
    printf("test_atVector_notEmptyVector\n");

    int_vector vec = createIntVector(4);
    int input_data[] = {1, 2, 3, 4};
    for (int i = 0; i < 4; i++) {
        pushToIntVector(&vec, input_data[i]);
    }

    int index = 2;

    printf("Element in position %d: %d (address %p)\n\n", index, getIntVectorValue(&vec, index), elementInIntVector(&vec, index));
}


void test_atVector_requestToLastElement() {
    printf("test_atVector_requestToLastElement\n");

    int_vector vec = createIntVector(4);
    int input_data[] = {1, 2, 3, 4};
    for (int i = 0; i < 4; i++) {
        pushToIntVector(&vec, input_data[i]);
    }

    int* elementPointer = elementInIntVector(&vec, 3);

    printf("Element in position 3: %d (address %p)\n\n", *elementPointer, elementPointer);
}


void test_back_oneElementInVector() {
    printf("test_back_oneElementInVector\n");

    int_vector vec = createIntVector(0);
    pushToIntVector(&vec, 5);

    int* lastElementPointer = lastInIntVector(&vec);

    printf("Element in last position: %d (address %p)\n\n", *lastElementPointer, lastElementPointer);
}


void test_front_oneElementInVector() {
    printf("test_front_oneElementInVector\n");

    int_vector vec = createIntVector(0);
    pushToIntVector(&vec, 5);

    int* firstElementPointer = firstInIntVector(&vec);

    printf("Element in first position: %d (address %p)\n\n", *firstElementPointer, firstElementPointer);
}


void test() {
    test_pushBack_emptyVector();
    test_pushBack_fullVector();
    test_popBack_notEmptyVector();
    test_atVector_notEmptyVector();
    test_atVector_requestToLastElement();
    test_back_oneElementInVector();
    test_front_oneElementInVector();
}


int main() {
    test();
}